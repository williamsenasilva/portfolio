let ZEPPELIN_FULLNAME = [
  "Zeppelin Systems Latin America",
  "Zeppelin Systems Latin America"
];
let TECSIDEL_FULLNAME = [
  "Tecsidel, Brazil",
  "Tecsidel do Brasil"
];
let PARTICULAR_PROJECT = [
  "Particular project",
  "Projeto particular"
];
let HOME = [
  "home",
  "home"
];
let MENU = [
  "menu",
  "menu"
];
let ABOUT = [
  "about",
  "sobre"
];
let GRADUATION = [
  "Education",
  "Educação"
];
let PORTFOLIO = [
  "portfolio",
  "portfolio"
];
let CONTACT = [
  "contact",
  "contato"
];
let SHORT = [
  "Short",
  "Resumo"
];
let SHORT_ME = [
  "Software Engineer | Electronic Technician",
  "Engenheiro de Software | Técnico em Eletrônica"
];
let BIO = [
  "Bio",
  "Bio"
];
let BIO_INTRO = [
  "Bachelor of Science in Computer Science. Bachelor of Science and Technology. Electronic Technician.\n" +
  "I am an enthusiast of Free Software and Internet of Things.",
  "Bacharel em Ciência e Tecnologia. Bacharel em Ciência da Computação. Técnico em Eletrônica.\n" +
  "Entusiasta de Software Livre e Internet das Coisas."
];
let BIO_SKILLS = [
  "Main skills: Linux, C/C++, Haskell, Java, Javascript, Python, Rust, Git, SVN, MongoDB, MySQL/MariaDB, PostgreSQL, CSS, Flask, HTML, Kivy, React Native, Electron, Docker, Docker Compose, Shell script",
  "Habilidades principais: Linux, C/C++, Haskell, Java, Javascript, Python, Rust, Git, SVN, MongoDB, MySQL/MariaDB, PostgreSQL, CSS, Flask, HTML, Kivy, React Native, Electron, Docker, Docker Compose, Shell script"
];
let CEAP_SHORT_NAME = [
  "CEAP",
  "CEAP"
];
let CEAP_FULL_NAME = [
  "Educational Assistance Center",
  "Centro Educacional Assistencial Profissionalizante"
];
let UFABC_SHORT_NAME = [
  "UFABC",
  "UFABC"
];
let UFABC_FULL_NAME = [
  "Federal University of ABC",
  "Universidade Federal do ABC"
];
let DEGREE_BCC = [
  "Bachelor of Computer Science at",
  "Bacharel em Ciência da Computação pela"
];
let DEGREE_BCT = [
  "Bachelor of Science and Technology at",
  "Bacharel em Ciência e Tecnologia pela"
];
let DEGREE_ELECTRONIC_TECHNICIAN = [
  "Electronic technician at",
  "Técnico em Eletrônica pelo "
];
let EXPERIENCE = [
  "Experience",
  "Experiência"
];
let EXPERIENCE_GFT = [
  "System Analyst at GFT",
  "Analista de Sistemas na GFT"
];
let EXPERIENCE_GFT_TIME = [
  "since 03/2022",
  "desde 03/2022"
];
let EXPERIENCE_TOTVS = [
  "System Analyst at at TOTVS",
  "Analista de Sistemas na TOTVS"
];
let EXPERIENCE_TOTVS_TIME = [
  "from 10/2019 to 03/2022",
  "de 10/2019 a 03/2022"
];
let EXPERIENCE_TECSIDEL = [
  "System Analyst at Tecsidel",
  "Analista de Sistemas na Tecsidel"
];
let EXPERIENCE_TECSIDEL_TIME = [
  "from 10/2018 to 10/2019",
  "de 10/2018 a 10/2019"
];
let EXPERIENCE_ZEPPELIN = [
  "Researcher at Zeppelin Systems Latin America",
  "Pesquisador na Zeppelin Systems Latin America"
];
let EXPERIENCE_ZEPPELIN_TIME = [
  "from 05/2015 to 12/2018",
  "de 05/2015 a 12/2018"
];
let EXPERIENCE_LSITEC = [
  "Researcher at LSI-TEC (USP)",
  "Pesquisador na LSI-TEC (USP)"
];
let EXPERIENCE_LSITEC_TIME = [
  "from 05/2014 to 04/2015",
  "de 05/2014 a 04/2015"
];
let CLOSE = [
  "Close",
  "Fechar"
];
let COMPANY = [
  "Company",
  "Empresa"
];
let VIDEO = [
  "Video",
  "Vídeo"
];
let IMAGE = [
  "Image",
  "Imagem"
];
let WEBSITE = [
  "website",
  "website"
];
let VERSIONING = [
  "Versioning",
  "Versionamento"
];
let TEAM = [
  "Team",
  "Equipe"
];
let ME = [
  "Me",
  "Eu"
];
let INTERN = [
  "Intern",
  "Estagiário"
];
let DEVELOPER = [
  "Developer",
  "Desenvolvedor"
];
let SOCIAL_MEDIAS = [
  "Social Medias",
  "Redes Sociais"
];
let MAIL = [
  "e-mail",
  "e-mail"
];
let WEB = [
  "Web",
  "Web"
];
let NAME = [
  "William Sena Silva",
  "William Sena Silva"
];
let INTELLI_FILTER = [
  "Intelli-filter",
  "Intelli-filter"
];
let INTELLI_FILTER_SHORT = [
  "The Intelli-filter is a programmable logic sequencer developed by \
  Zeppelin Systems LA. It is a system that aims to reduce the risk of work \
  predictable filter functionalities through the use of technologies in an \
  industrial environment. This device has the advantages of being installed in \
  a wide range of environments, which promotes the maintenance of industrial \
  filters in pneumatic systems and it is applicable to different situations. \
  Using Industry 4.0 and Internet of Things methodologies, Intelli-filter \
  was a study designed to work on a Wi-Fi network and send data from realtime \
  filter systems to an interactive database application, where it controls the \
  maximization and optimization of the system through the generated savings, \
  the filter’s status, among other information. All information collected on \
  each synchronization is stored on the Intelli-filter application and can be \
  viewed offline. You can also export data or store in the cloud or social \
  networks. This development found out that a process that took 30 minutes can \
  be reduced to a process of 30 seconds.",
  "O Intelli-filter é um sequenciador lógico programável. Um sistema que visa \
  reduzir o risco de acidentes de trabalho através do uso de tecnologias no \
  ambiente industrial. Este dispositivo tem a vantagem de poder ser instalado em uma \
  larga faixa de ambientes, o que facilita a manutenção de filtros industriais em \
  sistemas pneumáticos e isso é aplicável em diferentes situações. \
  Utilizando metodologias da Indústria 4.0 e Internet das Coisas, o Intelli-filter \
  foi projetado para funcionar numa rede Wi-Fi e enviar dados do filtro em tempo real para um \
  aplicativo Android, onde é possível visualizar a economia gerada, status do filtro, \
  entre outras informações. Todos os dados coletados de cada sincronização são armazenados no \
  aplicativo do Intelli-filter e podem ser visualizados de forma offline. Também é possível \
  exportar os dados ou salvá-los na nuvem ou compartilhá-los numa rede social. \
  Este desenvolvimento descobriu que o processo que antes durava 30 minutos pode ser \
  reduzido para um processo de 30 segundos."
];
let INTELLI_FILTER_VERSIONING_P = [
  "Each project was versioned using Git. Each project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described). \
  An extra repository was created for general documentation involving all projects.",
  "Cada projeto foi versionado utilizando o Git. Cada projeto tem também seu próprio repositório no \
  GitLab com seu CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation \
  é descrita). Um repositório extra foi criado para uma documentação geral envolvendo todos os projetos."
];
let TO_BOARD = [
  "to board",
  "para placa"
];
let SKILLS_TO_BOARD = [
  "electronic circuits development, design of PCB and schematics",
  "desenvolvimento de circuitos eletrônicos, design de PCB e esquemáticos"
];
let TO_SEQUENCER_FIRMWARE = [
  "to sequencer firmware",
  "para o firmware do sequenciador"
];
let SKILLS_TO_SEQUENCER_FIRMWARE = [
  "C/C++",
  "C/C++"
];
let TO_WIFI_FIRMWARE = [
  "to wifi firmware",
  "para o firmware do wifi",
];
let SKILLS_TO_WIFI_FIRMWARE = [
  "C/C++",
  "C/C++"
];
let TO_ANDROID_APP = [
  "to Android App",
  "para o aplicativo Android"
];
let SKILLS_TO_ANDROID_APP = [
  "Jquery Mobile, Javascript, HTML, CSS, SQL",
  "Jquery Mobile, Javascript, HTML, CSS, SQL"
];
let TO_WEB_SERVER = [
  "to web server",
  "para o servidor web"
];
let SKILLS_TO_WEB_SERVER = [
  "Linux, Apache, Python, Javascript, HTML, CSS, SQL",
  "Linux, Apache, Python, Javascript, HTML, CSS, SQL"
];
let INTELLI_FILTER_TASKS_ME = [
  "All projects development, documentation and tests",
  "Desenvolvimento de todos os projetos, testes e documentação"
];
let INTELLI_FILTER_TASKS_INTERN_1 = [
  "App design",
  "Design do App"
];
let INTELLI_FILTER_TASKS_INTERN_2 = [
  "Marketing",
  "Marketing"
];
let ZEPPDOCS = [
  "ZeppDocs",
  "ZeppDocs"
];
let ZEPPDOCS_SHORT = [
  "ZeppDocs is a site where customers can access the manuals of their projects. \
  The process for this is very simple. The customer uses their smartphone to read a qrcode. \
  This qrcode has a link to ZeppDocs. Once in ZeppDocs, the customer must fill out \
  fields of a form: first name, last name and corporate email. After that, the customer \
  click submit. Then an email is sent to the client's corporate email with a \
  link that gives access to the manual.",
  "O ZeppDocs é um site onde os clientes podem acessar os manuais dos seus projetos. \
  O processo para isso é muito simples. O cliente utiliza seu smartphone para ler um qrcode. \
  Este qrcode tem um link para o ZeppDocs. Uma vez no ZeppDocs, o cliente deve preencher \
  alguns campos de um formulário: nome, sobrenome e e-mail corporativo. Após isso, o cliente \
  clica em enviar. Então, um e-mail é enviado para o e-mail corporativo do cliente com um \
  link que dá acesso ao manual."
];
let ZEPPDOCS_VERSIONING_P = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let SKILLS_TO_ZEPPDOCS_WEB_SERVER = [
  "Linux, Apache, Python, Javascript, HTML, CSS, SQL",
  "Linux, Apache, Python, Javascript, HTML, CSS, SQL"
];
let ZEPPDOCS_TASKS_ME = [
  "Development (back-end and front-end), documentation and tests",
  "Desenvolvimento (back-end e front-end), documentação e testes"
];
let ZEPPDOCS_INTERN_1 = [
  "Development of front-end",
  "Desenvolvimento do front-end"
];
let ZEPPSHOP = [
  "ZeppShop",
  "ZeppShop"
];
let ZEPPSHOP_SHORT = [
  "ZeppShop is a virtual store for the sale of products, equipment and services of the \
  Zeppelin Systems Latin America.",
  "O ZeppShop é uma loja virtual para venda de produtos, equipamentos e serviços da \
  Zeppelin Systems Latin America."
];
let ZEPPSHOP_VERSIONING_P = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let SKILLS_TO_ZEPPSHOP_WEB_SERVER = [
  "Linux, Apache, PHP, Javascript, HTML, CSS, SQL",
  "Linux, Apache, PHP, Javascript, HTML, CSS, SQL"
];
let ZEPPSHOP_TASKS_ME = [
  "Development (back-end and front-end), documentation and tests",
  "Desenvolvimento (back-end e front-end), documentação e testes"
];
let GPIE = [
  "GPIE",
  "GPIE"
];
let GPIE_SHORT = [
  "GPIE is a web service that validates whether a document is released for publication. \
  All documents must undergo the GPIE validation process. Besides that, \
  GPIE has features that help designers and coordinators find documents \
  and products faster than the company's ERP.",
  "O GPIE é um serviço web que valida se um documento está liberado para ser publicado. \
  Todos os documentos devem passar pelo processo de validação do GPIE. Além disso, \
  o GPIE tem funcionalidades que ajudam os projetistas e coordenadores a encontrar documentos \
  e produtos de forma mais rápida que o ERP da empresa."
];
let GPIE_VERSIONING_P = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let SKILLS_TO_GPIE_WEB_SERVER = [
  "Linux, Apache, Python, Javascript, HTML, CSS, SQL",
  "Linux, Apache, Python, Javascript, HTML, CSS, SQL"
];
let GPIE_TASKS_ME = [
  "Development (back-end and front-end), documentation and tests",
  "Desenvolvimento (back-end e front-end), documentação e testes"
];
let TITOGI = [
  "titogi",
  "titogi"
];
let TITOGI_SHORT = [
  "Titogi is an online study environment that helps students through \
  exercises solved step by step. It is 100% free.",
  "O Titogi é um ambiente de estudos online que ajuda estudantes através de \
  exercícios resolvidos passo a passo. É 100% gratuito."
];
let TITOGI_VERSIONING_P = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let SKILLS_TO_TITOGI_WEB_SERVER = [
  "Linux, Apache, Node.js, Javascript, HTML, CSS, NoSQL",
  "Linux, Apache, Node.js, Javascript, HTML, CSS, NoSQL"
];
let TITOGI_TASKS_ME = [
  "Development (back-end and front-end), documentation and tests",
  "Desenvolvimento (back-end e front-end), documentação e testes"
];
let TITOGI_TASKS_DEVELOPER_1 = [
  "Development of front-end",
  "Desenvolvimento do front-end"
];
let TITOGI_TASKS_DEVELOPER_2 = [
  "Development of front-end",
  "Desenvolvimento do front-end"
];
let IGRTOKEN = [
  "IGR-TOKEN",
  "IGR-TOKEN"
];
let IGRTOKEN_SHORT = [
  "IGR-Token is a App where you can easily obtain the url for the ingredient certificate for any specific \
  recipe based product. All you need is to capture the GTIN-13 (product BarCode) and add the latest \
  date shown on the individual package. The App will query the Blockchain for the IGR token if the recipe \
  includes any certified ingredient.",
  "IGR-Token é um App que permite ao usuário obter a url para o certificado do ingrediente de uma específica receita de \
  produto. Tudo que você precisa fazer é capturar o GTIN-13 (código de barras do produto) e \
  adicionar a data de validade do produto. O App consultará o Blockchain para obter o IGR Token se a \
  receita incluir um ingrediente certificado."
];
let IGRTOKEN_VERSIONING_P = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let SKILLS_TO_IGRTOKEN_ANDROID_APP = [
  "Versão >= 1.0.0: React Native, Javascript, HTML, CSS. Versão < 1.0.0: Python, Kivy",
  "Version >= 1.0.0: React Native, Javascript, HTML, CSS. Version < 1.0.0: Python, Kivy"
];
let IGRTOKEN_TASKS_ME = [
  "Development, documentation and tests",
  "Desenvolvimento, testes e documentação"
];
let SGP = [
  "ROAD MANAGEMENT SYSTEM",
  "SISTEMA DE GESTÃO DE PISTAS"
];
let SGP_SHORT = [
  "SGP is a App where users can management all lanes added on app. Its user interface help users to allow or pay vehicles that has any problem ontag detection.",
  "SGP é um aplicativo no qual os usuários podem gerenciar todas as faixas adicionadas no aplicativo. Sua interface de usuário ajuda os usuários a permitir ou pagar veículos com qualquer problema na detecção de etiquetas."
];
let SGP_VERSIONING = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let SKILLS_TO_SGP_ANDROID_APP = [
  "React Native, Javascript, HTML, CSS.",
  "React Native, Javascript, HTML, CSS."
];
let SGP_TASKS_ME = [
  "Development, documentation and tests",
  "Desenvolvimento, testes e documentação"
];
let NVMSIM = [
  "NVMSim",
  "NVMSim"
];
let NVMSIM_SHORT = [
  "The NVMSim (Non-volatile memory simulator) is based on the integration of a simulator of hardware architecture with a simulator of memories with \
  NVMs support, Sniper and NVMain, respectively.",
  "O NVMSim (Non-volatile memory simulator) é baseado na integração de um simulador de arquitetura de hardware com um simulador de memórias com suporte \
  a NVM, Sniper e NVMain, respectivamente."
];
let SKILLS_TO_NVMSIM = [
  "C/C++, Python, Docker, Docker-Compose, Shell Script",
  "C/C++, Python, Docker, Docker-Compose, Shell Script"
];
let NVMSIM_VERSIONING = [
  "The project was versioned using Git. The project also has its own repository in GitLab with its \
  CHANGELOG (where each improvement is described) and a Wiki (where a specific documentation is described).",
  "O projeto foi versionado utilizando o Git. O projeto tem também seu próprio repositório no GitLab com seu \
  CHANGELOG (onde cada melhoria é descrita) e uma Wiki (onde uma específica documentation é descrita)."
];
let NVMSIM_TASKS_ME = [
  "Development, documentation and tests",
  "Desenvolvimento, testes e documentação"
];
let SKILLS_NEEDED = [
  "Skills Needed",
  "Habilidades Necessárias"
];
